Iceberg de Kaboom Estudio
-------------------------

-   "Hazme reír" de un medio lleno de lolcows
-   El Eisner de Oscar
-   La plumilla de plata
-   Best Buddies gate (lamentablemente no podría juntar todo lo que
    existe)
-   OGL vs. Gantus (OGG no es el autor de Hermelinda Linda, y como
    respondieron en Katun Media)
-   Las mentiras (y las que no son tanto) del currículum de Hoacho
-   La Secta (tengo la impresión de que solo de este punto sería un
    video completo)
-   Los trabajos comprobables de Óscar
-   Krónikas Kuadradas
-   Blue Demon Jr.: El Legado
-   RBD
-   Humberto Ramos, Micro y otros más empezaron en Ka-Boom
-   Jose Karlos
-   Rodo (no sé si sea buena idea)
    -   Paco Meison
-   El trolletariado
-   Enfermedades
-   El sexo dentro de Kaboom
-   Creadores de Karmatron
-   El comic de Fox
-   La muerte de Óscar Gonzalez Guerrero
-   La Rocola del Tío Hoacho
-   Dibujante(s) oficial(es) de los Simpsons
-   Wikipedia e Inciclopedia

Introducción
------------

Ah, Ka-Boom estudio. Una editorial *sui-generis*. Es uno de esos grupos
que amas u odias. Pero en gran medida, y con el paso de los años, la
cantidad de gente que los odiaba o los consideraba una fuente de risas
inagotable terminó siendo mucho mayor a los que los apreciaban de una u
otra manera.

Ha pasado más de un año de la muerte de Óscar Gonzalez Loyo, líder
indiscutible del proyecto K! Para ser honesto, la labor del
*trolletariado* ya tenía varios años (algunos años incluso antes de la
muerte de Oscar) que había dejado de ser una llamada de atención para
evitar que nueva gente cayera en sus garras. Con la existencia de
Internet y de proyectos como *Kronikas Oblongas del Maestro Nath* o *No
Creo en Karmatron*, era muy complicado para esta editorial el poder
cautivar a nuevos elementos.

La realidad es que si bien el tema Ka-Boom ya no tiene demasiado sobre
lo que se pueda hablar, sobre todo en su actualidad. Pero también es
cierto que muchos temas se quedaron en el tintero. Y también es cierto
que se puede revisar el pasado de la editorial y recordar tiempos en los
que Ka-Boom era relevante.

Ka-Boom aún tiene cosas por darnos, pero es practicamente un hecho que
podrán enterarse sobre ellas en otros medios como el mismo NCK, como el
blog de Karen Greer, como el canal de Hanzo si es que un día decide
regresar con su proyecto de Mermatron y los transtornados.

(De momento no tengo un órden en el iceberg, ya veremos como queda
organizado)

Hazme reir en un medio lleno de lolcows
---------------------------------------

Es bien sabido que el comic mexicano es un medio... muy particular. Si
bien contamos con dibujantes que la han hecho en grande fuera del país,
como son los casos de Humberto Ramos, José Omar Ladronn, Tony Sandoval y
Paco Medina entre muchos otros, también es un lugar lleno de historias
que bien podrían ser comentadas en otro(s) video(s).

La importancia de este punto recae en lo que ha representado Kaboom
dentro de la industria del comic. Parafraseando a Luis Gantús (otra gran
personalidad :v), el comic mexicano siempre fue un medio desunido en el
que los conflictos se olvidaban momentáneamente al existir OGL y los
Ka-Boom.

Cabe decir que esto nunca fue gratuito, Oscar y los K tenían la
costumbre de despotricar contra quienes osaran criticarlos en medios
como Facebook, su blog Emelkin y el infame Kuantika radio/Katun Media.

Así pues, al contestar agresiones de Oscar y compañía, varios dibujantes
que no se llevaban tan bien encontraban un poco de empatía, y nos
querían hacer creer que en realidad eran un gremio muy unido. Porque lo
son, ¿cierto?

El Eisner de Oscar
------------------

En el año de 1999 Bongo Comics publicaba "Treehouse of Horror \#\#5" o
"La casita del Horror No. 5" que contaba con la participación de Jill
Thompson, Sergio Aragonés, Scott Shaw, entre otros. Y entre esos otros
se encontraba nuestro Monero Favorito. Oscar Gonzalez Loyo.

La historia que Oscar dibujó fue escrita por Jill Thompson y se llama
"Dark Lisa", una pequeña historia sobre Lisa adquiriendo poderes
telequinéticos tras ingerir pescado en la cafetería escolar. Contiene
uno de los mejores páneles que haya visto en cualquier historieta de los
Simpsons, mostrando a Lisa como una especie de Dark Phoenix. Tengo que
admitir que es una historia muy buena, y bastante bien dibujada.

En algún momento (que no sé cuando vaya a llegar) podría hacer una
reseña del comic completo.

A resumidas cuentas, en el año 2000 este comic se llevó el Premio Eisner
por mejor publicación de humor. Esto es un hecho innegable. Se puede
comentar o no si el premio fue de rebote, aunque en mi honesta
impresión, el arte tiene un momento muy destacable en el ya mencionado
panel. Si bien es cierto que la misma Bongo Comics no reconoce en la
portada a OGL.

Las historias sobre como es que recibió su premio varían dependiendo a
la persona que le pregunten. Lo que es una realidad es que Oscar no
habría vuelto a ser si quiera mencionado si fuera verdad eso de que robó
la placa. Fue homenajeado por la misma página de la comic-con, aunque la
redacción pareciera haber sido hecha por un becario.

Lo que sí es verdad, es que aparentemente Oscar y Horacio no salieron de
la mejor forma de Bongo Comics. Pero eso lo hablaré en otro momento.

La plumilla de plata
--------------------

No hay mucho que decir en este respecto. Fue una entrega de premios
organizada en el año 2003 por Ka-Boom estudio.

Según lo relatado en el blog del monero enmascarado, la plumilla de
plata es un trofeo creado en base a un diseño de Oscar Gonzalez Loyo y
fue entregado a diversas personalidades por su aporte al arte
secuencial.

Los comiqueros: Sergio Aragonés, Francois Boucq, Tony Brancford, Juan
Jiménez, Mike Kunkel, Carlos Meglia, Humberto Ramos, Stan Sakai, Philip
Victor, Phill Yeh y Sandra Hope.

También hubo artistas del doblaje que ganaron la presea: Gabriel Chávez,
Rosanelda Aguirre, Silvia Garcé, Gonzalo Curiel, Guadalupe Novel,
Angelita Villanueva, Alejandro Villeri, Carlos Segundo y Juan Antonio
Edwards.

Y algunas tiendas de comics: Comicastle, Librería Graphis y Nostromo
Cómics.

Los pertenecientes a la vieja guardia del cómic mexicano:Óscar González
Guerrero, Sixto Valencia, dibujante de Memín Pinguín, Rubén Lara, y
Zenaido Velásquez.

Algunos que faltaron del primer grupo: Ricardo Arnaiz, y Polo Jasso.

El segundo día les toco a Ramón Valdiosera, Juan Alba y Ángel Mora, pero
estos no estuvieron para recibir su premio.

Posteriormente, recibieron su trofeo Nicanor Peña, Arturo Saíd, Alberto
Hinojosa, y el Taekwondoin Óscar Mendiola.

También se le entregó a Klaus Sapien, Miguel Ángel García, Joaquín
González y Benjamín González, Dulce Leyva, los miembros de Anicast, y
los integrantes de La perrera, Ricardo Peláez, Cyntia Bolio, Patricio
Betteo, Ricardo García y Eric Proaño.

A pesar de estar en desacuerdo Óscar González Loyo también ganó la
"Plumilla de Plata", misma que fue entregada por su esposa
sorpresivamente, este fue un emotivo momento donde el sorprendido monero
nos pidió no dejar de soñar.

Best buddies gate
-----------------

Este es uno de los puntos más tristemente célebres de los K!

En resumen, Best Buddies publicó en el año 2013 una convocatoria para
participar en un evento en el cual participarían jovenes de Best Buddies
para realizar un comic sobre el Bullying. Hasta aquí todo bien. Lo
extraño eran algunas de las condiciones de la convocatoria en las que
los participantes:

-   Tenían que participar con un seudónimo.
-   Debían crear personajes en específico para este comic.
-   La historia debía ser dibujada en opalina importada.
-   Terminaban cediendo los derechos de la obra al 100% en caso de una
    eventual publicación.

Todos estos puntos son altamente controversiales. Sobre todo si tomamos
en cuenta que se pierde cualquier derecho a posible remuneración.

Cuando hubo algunas quejas, Best Buddies mencionó que la convocatoria
fue redactada por el jurado. Que para variar eran los miembros de
Ka-Boom estudio.

La controversia se dio en un post de la página de Facebook llamada
Marvel Comics Mexico, la cual criticó la convocatoria. Esto provocó que
gente del medio, trolls y K! se embarcaran en una interezZZante
discusión al respecto. Con la gente del medio cuestionando la moralidad
del jurado y las bases de la convocatoria, mientras que Oscar y los K!
se centraban en mostrarlo todo como una muestra de la mala leche que se
les tenía. El problema fue la soberbia con la que respondía Susana
Romero, y el resbalón que tuvo Tonatiuh Rocha, al referirse a uno de los
trolls como su "Retard" de confianza. Un resbalón que le costaría caro a
Ka-Boom. A las pocas horas, Best Buddies anunciaba que se cancelaba la
convocatoria.

Para leer más al respecto, en la descripción dejaré los enlaces
dedicados al tema en el blog de Karen Greer.

OGL vs. Gantus.
---------------

Luis Gantus era el villano favorito de OGL. Generalmente era a quien
culpaba de todos los problemas que los K! enfrentaban contra la
industria del comic.

La importancia de Gantus dentro de la industria se resume en su cualidad
como organizador de exposiciones y su labor como historiador del comic
mexicano. Si bien puede ser criticable que únicamente considere
importante a quienes entren dentro de su grupo de contactos cercanos.
Este es un tema para otro momento. Nos concentraremos en la rivalidad y
el punto en el cual OGL despotricó en contra de Luis.

El punto más álgido fue cuando Luis demostró que la autoría del comic
Hermelinda Linda no correspondía a Don OGG (padre de OGL). Esto provocó
que en su programa de radio Oscar despotricara contra Luis Gantús y
varios elementos del gremio del comic mexicano.

El audio en su totalidad lo pueden encontrar en el canal
*CarlosArizpeLive*, y es muy recomendable escucharlo, para conocer a
Kaboom en su máxima expresión.

Las mentiras (y las que no son tanto) del curriculum de Hoacho
--------------------------------------------------------------

Horacio Sandoval siempre fue uno de los miembros favoritos de OGL dentro
de Ka-Boom estudio. Siempre fue tratado como alguien con una jerarquía
superior al resto de dibujantes Ka-Boom, y en el mismo nivel jerarquico
que Susana Romero.

Se ha mencionado de diferentes fuentes las cualidades como dibujante que
tenía Horacio. Sabemos que siempre se ha presentado como un dibujante
oficial de Los Simpsons, de Dragon Ball (directamente con Akira
Toriyama), que dibujó algunos números de "/Los Muppets Babies/" en
editorial VID. Y que también participó en la infame adaptación de "Los
Supercampeones del futbol" que publicó Editorial Toukan. Se comenta
incluso que rechazó la posibilidad de dibujar "Batman Adventures" en DC
Comics por quedarse en México e impulsar su estudio.

De esto es cierto que llegó a dibujar algunos números de *Simpsons
Comics*. Mi teoría al respecto es que Horacio fue el que consiguió el
trabajo con Bongo, y tal y como llegaron a planteárselo en el estudio a
Micro con el comic de *Las Chicas Superpoderosas*, Óscar se agandalló
algunos números. Repito, esto fue simplemente una teoría, y no es algo
que me conste. Su dibujo como maquilador es decente, y no hay nada que
lo distinga de algún otro dibujante genérico de ese comic. En Bongo, era
en ciertos números especiales donde sacaban la carne al asador
contratando diferentes escritores y dibujantes más reconocidos, como fue
en el caso de Treehouse of Horror. Que haya tenido trato directo con
Matt Groening, pues no hay pruebas de ello.

Sobre Dragon Ball, es sabido que dibujó unos libros para colorear, pero
a lo mucho contaban con la supervisión de TOEI. Un vistazo muy
superficial, y que le permitía algunos errores (que no sé si mostraré en
pantalla en este momento).

En el caso de "Los Supercampeones del Futbol", contrario a lo que
algunos trataron de negar, el mismo Jorge Break llegó a comentar que
Horacio sí trabajó en esa historieta, pero solo fue por algunos números
y en calidad de suplente. No recuerdo más detalles, pero jamás negó la
participación (e intento que hubo por quitarle el trabajo a Break) de
Horacio Sandoval.

Y sobre *Batman Adventures*, solo hay testimonios de gente cercana (y
que ya no lo es) de haber visto los dibujos de Hoacho, y su testimonio
respectivo del porqué no aceptó el trabajo.

Viene una opinión impopular. Si Horacio hubiera aceptado la ayuda de su
hermano Gerardo, quizá estaríamos hablando de que Horacio habría jugado
en las grandes ligas. DC Comics, Marvel Comics, Image. Sin embargo, no
hubiera sido al 100% un logro propio. Si bien el trabajo de Horacio
tiene sus inconsistencias, no hubiera sido un impedimento para tener
trabajo en alguna de estas editoriales. Pero creo que habría tenido la
carga mental de que no era del todo mérito de él haber triunfado en el
extranjero.

A veces me resulta extraño que se critique el nepotismo en otras áreas,
pero se justificara en estas instancias con el fin de criticar a
Ka-Boom.

¿Creo yo que el trabajo de Horacio es bueno o malo? Sin saber de dibujo,
sé que los números de Karmatron en los que trabajó son bien recibidos
por los fans. Su trabajo dentro de los Simpsons era cumplidor sin ser
espectacular.

Hablando de trabajos propios, su tira en Kronikas Kuadradas conocida
como "Hoachinton D.C. (Desastre Completo)" le hacía honor a su
subtítulo. Era un "completo desastre". Y objetivamente dentro de las
tiras que hacía dentro de la serie principal de "Kronikas Kuadradas"
había una o dos excepciones que podían ser consideradas decentemente
dibujadas, pero por lo regular, su trabajo propio es inconsistente.

Sabiendo esto, mi conclusión sería que Horacio es un buen maquilador,
pero no es bueno como creador original. Si hubiera llegado a las Grandes
Ligas, se hubiera quedado como un dibujante más del montón. Pero al
menos habría tenido algo qué platicarle a sus hijos.

La Secta
--------

Este es un tema que sin problemas puede abarcar un video y bastante
largo. Porque durante años hemos escuchado un sin fin de discusiones
sobre si Ka-Boom es una secta o no.

Los que defienden el sí, argumentan actitudes características de las
sectas, sobre el culto a la personalidad del líder, el limitar el
vestir, la forma en la que los miembros del estudio son alimentados, la
restricción a la individualidad "por el bien del grupo". También sabemos
de entrevistas y oportunidades de empleo que les brindaron y ellos
mismos rechazaron "por no incluir a todos los miembros del estudio". Y
también hay testimonio de como es que llegaron a alejar a varios
miembros de su familia. Incluso la prohibición tajante de asistir a
funerales de familiares cercanos.

Quienes argumentan el no, hablan de que los miembros podían salir a
voluntad del lugar, y Mariana es una muestra de ello. Se habla también
de que el actuar es más bien el de una comuna, lo cual tampoco mejora
demasiado la reputación del estudio.

En su momento habrá un video tocando este tema, pero simplemente será
recopilatorio de los comentarios en Pro y en Contra.

Los trabajos comprobables de Oscar
----------------------------------

Como (probablemente) ya se comentó en este mismo video, Óscar gozó de
cierto éxito en los años ochenta. Podemos cuestionar si fue o no con la
ayuda del señor Óscar Gonzalez Guerrero. De cualquier modo, Parchís y
Karmatrón son historietas que gozaron de un considerable éxito en los
años ochenta.

Honor a quien honor merece, la manera en la que algunos de sus fans lo
despidieron al momento de su fallecimiento y el como resaltaban su obra
es algo digno de ser reconocido.

Oscar tuvo en mente algunos proyectos que sí vieron la luz, como el
infame "Mundo de Joe", las "Krónikas Kuadradas", Saskunah, entre otros.
Por su parte, fue contraportadista de "Las Aventuras de Capulina",
realizó algunos números de "Katy La Oruga" y las portadas para las
versiones estadounidenses de VHS de series como "Astroboy", "Kimba, el
león blanco" y "Gigantor".

Posteriormente, trabajaría para Bongo Comics haciendo tiras para Los
Simpsons de 2000 a 2002.

Entre 1996 y 2000 fue director de animación de Plaza Sésamo.

Sin ser prolífica, resulta interesante ver que los trabajos de OGL no
son tan pocos como se nos ha insistido que fue. *aparece portada de
Operación Bolívar*

Las Krónikas Kuadradas
----------------------

Krónikas Kuadradas fue un proyecto de webcomic de parte de Kaboom en el
que los dibujantes principales del estudio tenían participación.

Constaba de cinco series. Cuatro de ellas eran proyecto personal de su
autor, y la serie principal "Kronikas Kuadradas" contaba con la
participación de todos ellos. Cada semana se turnaban (o no) el guion y
el dibujo de una tira. Las series eran:

-   El mundo de Joe (Por Óscar Gonzalez Loyo).
-   Hoachinton D.C. (Desastre Completo) por Horacio Sandoval.
-   Kronikas Kuadradas (Kaboom Estudio)
-   Palomitas Country (Por Alejandro Palomares)
-   Mayi (Por Mariana Moreno)

Cada una de estas tiras era publicada en un sitio web conocido cuya
dirección era "kronikaskuadradas.com.mx". Como dato anecdótico, el blog
del Maestro Nath (ahora conocido como Crónicas Oblongas del Maestro
Nath) contaba con la dirección "kronikaskuadradas.com". Un poco
inspirados por eso, supongo que los dueños del sitio "El Crónico" (del
que llegué a hablar en un video) se inspiraron a comprar el dominio
"oscargonzalezloyo.com". Es triste, pero "El Crónico" ya no existe en la
red. Y el blog del Maestro Nath ahora es accesible vía su url en
blogspot.

Aunque por otro lado, las cosas se equilibran. El dominio
"kronikaskuadradas.com" actualmente redirige a la página principal de
Kaboom estudio, lo cual implica que el acceso a sus tiras se ha perdido.
Pero aún se puede acceder a una buena cantidad de sus tiras usando
"Wayback Machine" y escribiendo "kronikaskuadradas.com.mx".

Volviendo al tema, las tiras fueron duramente criticadas y con todo el
derecho del mundo:

-   "El mundo de Joe" estaba llena de filosofía kundalini, como la que
    tanto pregona Oscar. Él siempre mencionó que su tira era un intento
    por criticas la pseudointelectualidad de sus ex-compañeros
    universitarios, al igual que la de sus compañeros artistas del comic
    mexicano. Para lo cual fracasó miserablemente. El Perro Joe es
    inmamable, y si bien es cierto que los personajes que representan a
    otros moneros también lo son, dentro de la misma obra llegan a ser
    menos pedantes que el mismo perro güevón.

-   "Hoachinton D.C. (Desastre completo)" es, con mucho, la peor de
    todas las tiras. Sobre la serie principal y El Mundo de Joe se puede
    decir que estaban hechas para provocar cierta reacción en los
    detractores que la leyeran. Palomitas Country y Mayi eran más
    visitadas por lectores casuales que no sabían nada del mundillo del
    comic mexicano, y dentro de lo que cabe, rara vez se metían en
    polémicas. Pero la obra de Horacio era tan decepcionante que casi
    nunca era mencionada de una u otra forma, salvo para recalcar lo
    mala que era.

Si algo bueno se puede decir es que muy pocas veces tocaba temas
referentes al conflicto de Kaboom con el resto del mundo, y que de
cierta manera sí representaban una obra del autor. Aunque pensándolo
bien, con esa calidad mejor que ni se ayude él solito.

-   Kronikas Kuadradas era la serie principal. Y, a su manera, trataba
    de los obstáculos que Kaboom Estudio decía que encontraba en su
    camino de crear una industria del comic mexicano. La verdad sea
    dicha, eran muy raras las veces en las que sus críticas eran
    válidas. Las desventuras que les pasaban en muchas ocasiones son
    causadas por la misma soberbia que incluso llegaban a plasmar en sus
    personajes.

Sin embargo, ese no era el mayor problema de esta tira. El problema era
entrar a la sección de comentarios y encontrar eternas peleas que poco o
nada tenían que ver con el comic en cuestión. Y no estoy hablando del
derecho de criticar una tira que había de parte de los detractores, que
en ocasiones sí se centraban en el punto principal de la tira. Hablo
también de la arrogancia en las respuestas de parte de Kaboom, sacando a
relucir temas personales con la persona que les comentaba.

En resumen, estas tiras eran solo un descargue de frustración en la
mayoría de los casos, y si la idea era atraer nuevos lectores, iba a ser
bastante difícil con una tira basada 100% en chistes locales.

A pesar de todo, en el año 2016 Kaboom publicó una versión impresa de
esta tira, siendo esta la única en ser impresa por parte de KaBoom
estudio. No fue para nada bien recibida, ni por los fans más acérrimos
del estudio (que decían comprarla, pero solo por apoyar y en espera de
más Karmatrón).

-   "Palomitas Country" era la tira de Alejandro Palomares. La cual, sin
    lugar a dudas, era la que más se trataba de un trabajo personal del
    autor que una descarga. La obra pasó por dos etapas. Una en la que
    era ayudado por Mauricio Cosío, y se notaba que estaba a gusto
    haciendo sus historias. En un inicio eran miniseries de tiras
    divididas entre 3 y 5 partes cada una, y eran bastante digeribles y
    muy accesibles a un público casual.

La segunda etapa viene a raíz de la salida de Mauricio Cosío de la
editorial. Y en la obra se nota el impacto de esta salida. Hay un bajón
de calidad en cuanto a guion. Y sus historias eran autocontenidas (de
solo una tira). Aún así, nunca dejaron de ser digeribles. A pesar que
cierta parte del trolletariado se encargaba de hacer un sin fin de
chistes sobre el complejo de Edipo.

Esta tira es, de manera evidente, la que mejor/peor suerte tuvo de
todas. Por un lado fue sacada de la página principal al momento que
Alejandro Palomares decidió dejar el estudio. Por otro, sabemos que
Palomitas Country sí que llegó a ser publicada en Francia. Un pequeño
gran triunfo para Alejandro. Una historia que quizá se mencione en otro
vídeo.

-   Por último "Mayi" es la tira de Mariana Moreno, y que tuvo una
    recepción mixta. Si bien lograba atraer fans casuales (tenía más
    éxito en eso que "Palomitas Country\") sí tenía una calidad un tanto
    más irregular. Por momentos tenía tiras muy buenas (como su
    crossover con Karmatrón), y tiras muy malas (como aquella en la que
    al quemarse con el sol, fue pionera en los"blackfaces\" dentro del
    webcomic).

De cualquier manera, en ciertas ocasiones le resultaba muy difícil no
meterse en polémicas del estudio. Sin embargo, nadie podrá negar que
Mayi era con bastante diferencia la tira que más daba de qué hablar.
Tanto para bien, como para mal. Y también era la tira a la que mejor le
iba semanalmente fuera del asunto de "El mundo vs. OGL". Y eso era
bueno. Mejor que estar discutiendo por largos párrafos sobre la
historieta de un perro huevón.

Blue Demon Jr. El legado
------------------------

"Blue Demon Jr.: El Legado" fue un comic escrito por Susana Romero y
dibujado por Horacio Sandoval que fue publicado en el año 2005 por
Kaboom Estudio.

En su momento fue presentado como "la gran reactivación de la industria
del comic mexicano".

Karmatron por encima de todo
----------------------------

En los años ochenta, Oscar logró un éxito bastante considerable en la
época en la que el comic mexicano sí llegó a ser una industria. Fue
dibujante de portadas de "Las Aventuras de Capulina", y de la historieta
de "Parchís", la cual tuvo un moderado éxito en su momento.

Sin embargo, el trabajo por el que Óscar es más conocido es Karmatrón y
Los Transformables, el cual fue un comic que se puede considerar como
uno de los más importantes en la historia del comic mexicano, cuya
publicación llegó a los 298 números.

Algo que se puede decir en favor del comic, es que tiene una base de
seguidores, que si bien no puede decirse muy grande, sí que es bastante
fiel. Son personas que se mostraron muy agradecidas con Oscar González
Loyo por haber creado esta historia.

Ahora bien, sobre la calidad del comic, es algo que va a variar
dependiendo a quién le pregunten. A mi modo de ver, basándome en
diálogos y dibujos no es algo que yo leería con demasiado agrado, pero
eso no significa que el comic sea malo. De hecho algunos números son
bastante decentes. Si alguien está interesado, puede ver/leer algunos de
ellos en el canal de Punk Karmageek.

En mi opinión personal, el comic tuvo el gran tino de tomar varias de
las historias que estaban en boga por aquella época, como fue Star Wars,
Star trek, Los transformers y los mangas de mechas de la época de los
ochenta. Si bien el dibujo no era extraordinario, por lo menos sí hacía
fluída la lectura. Sé que muchos de ustedes van a diferir al respecto,
pero se dejaba leer, y eso es mucho más que el promedio del comic
mexicano.En los años ochenta, Oscar logró un éxito bastante considerable
en la época en la que el comic mexicano sí llegó a ser una industria.
Fue dibujante de contraportadas de "Las Aventuras de Capulina", y de la
historieta de "Parchís", la cual tuvo un moderado éxito en su momento.

Sin embargo, el trabajo por el que Óscar es más conocido es Karmatrón y
Los Transformables, el cual fue un comic que se puede considerar como
uno de los más importantes en la historia del comic mexicano, cuya
publicación llegó a los 298 números. Comenzó a ser publicado en febrero
de 1986, y continuaría hasta mediados del año 1991.

A inicios de los años dos mil, específicamente en el mes de diciembre de
2002, se publicó el primer número de lo que se conoce como la "nueva
era" de Karmatron, que es una especie de reboot/retcon del Karmatrón de
la vieja era. Comic que jamás tuvo una conclusión en todo este tiempo. A
la fecha se han publicado 20 números de esta era, y por lo que ha
mencionado el estudio, la producción de esta historia continuará y será
publicada a través de la nueva plataforma digital de Ka-boom, que está
próxima a ser lanzada.

Algo que se puede decir en favor del comic, es que tiene una base de
seguidores, que si bien no puede decirse muy grande, sí que es bastante
fiel. Son personas que se mostraron muy agradecidas con Oscar González
Loyo por haber creado esta historia.

Ahora bien, sobre la calidad del comic, es algo que va a variar
dependiendo a quién le pregunten. A mi modo de ver, basándome en
diálogos y dibujos no es algo que yo leería con demasiado agrado, pero
eso no significa que el comic sea malo. De hecho algunos números son
bastante decentes. Si alguien está interesado, existen canales en
Youtube que tienen vídeos en los que se leen algunos números del
Karmatrón de la vieja era.

En mi opinión personal, el comic tuvo el gran tino de tomar varias de
las historias que estaban en boga por aquella época, como fue Star Wars,
Star trek, Los transformers y los mangas de mechas de la época de los
ochenta. Si bien el dibujo no era extraordinario, por lo menos sí hacía
fluída la lectura. Sé que muchos de ustedes van a diferir al respecto,
pero se dejaba leer, y eso es mucho más que el promedio del comic
mexicano.

Sobre la nueva era, no puedo decir mucho, ya que está escrita
principalmente para los fans de la historia original. Esto es un
obstáculo para todos aquellos que quieren conocer de Karmatrón mediante
esta historia. Si bien tiene algunos buenos momentos, la lectura se
torna muy densa, y Oscar cometió el error de escribir diálogos sumamente
largos y tediosos que no ayudan para nada a leer el cómic. Cierto es que
la filosofía kundalini forma parte del contenido de Karmatrón y resulta
importante para la historia, pero la realidad es que lo convierte en un
comic sumamente aburrido. Tal vez hubiera sido preferible que terminaran
la vieja era, en vez de hacer una nueva era que no engancha tanto ni a
los fans más acérrimos.

Por otro lado, independiente de la opinión que se tenga sobre esta
historia, está lo que provocó en la forma de trabajar del estudio. Oscar
hizo que Ka-boom Estudio girara alrededor de la publicación de
Karmatrón. Por encima de los proyectos personales, por el trabajo
individual y hasta por la salud de varios de los miembros del estudio.
Aprovechándose del gusto de los fans por su historia, Óscar siempre le
brindó prioridad a la historieta del gigante kundalini, ya que siempre
hubo fans que estuvieron dispuestos a brindarle ayuda económica al
estudio con el afán de ver publicados los siguientes números de
Karmatrón. Números que iban saliendo a cuenta gotas.

No hay constancia de que a los artistas del estudio se les impidiera
realizar sus propias obras, pero es evidente que siempre quedaron en
segundo plano con tal de que la columna vertebral del estudio fuera el
siguiente número de Karmatrón y los transformables.

Dibujantes oficiales de los Simpsons
------------------------------------
